moduleLoader.imports("vector", [], function () {

	var returnObject = {};

	var vector = function (x, y) {

		if (x || y) {
			this.vx = x;
			this.vy = y;
		}

		return this;

	};

	var scale = function (scale) {
		this.vx *= scale;
		this.vy *= scale;
		return this;
	};

	var	add = function (vec2) {
		this.vx += vec2.vx;
		this.vy += vec2.vy;
		return this;
	};

	var	sub = function (vec2) {
		this.vx -= vec2.vx;
		this.vy -= vec2.vy;
		return this;
	};

	var	negate = function () {
		this.vx = -this.vx;
		this.vy = -this.vy;
		return this;
	};

	var	length = function () {
		return Math.sqrt(this.vx * this.vx + this.vy * this.vy);
		return this;
	};

	var lengthSquared = function () {
		return this.vx * this.vx + this.vy * this.vy;
		return this;
	};

	var normalize = function () {
		var len = this.length();
		if (len) {
			this.vx /= len;
			this.vy /= len;
		}
		return this;
	};

	var rotate = function (angle) {
		var vx = this.vx,
			vy = this.vy,
			cosVal = Math.cos(angle),
			sinVal = Math.sin(angle);
		this.vx = vx * cosVal - vy * sinVal;
		this.vy = vx * sinVal + vy * cosVal;
		return this;
	};

	var toString = function () {
		return '(' + this.vx.toFixed(3) + ',' + this.vy.toFixed(3) + ')';
	};
	
	returnObject = function () { return vector() };

	vector.scale = scale;
	vector.add = add;
	vector.sub = sub;
	vector.negate = negate;
	vector.length = length;
	vector.lengthSquared = lengthSquared;
	vector.normalize = normalize;
	vector.rotate = rotate;

	returnObject.vector = vector;

	return returnObject;

});
