moduleLoader.imports("canvas", [], function () {
	
	returnObject = function () { return getCanvas() };

	returnObject.id = 'stage';

	var canvas = document.getElementById(returnObject.id);
	var ctx = canvas.getContext('2d');

	var getCanvas = function () { return canvas };

	var getContext = function () { return ctx };

	returnObject.getContext = function () { return getContext() };

	return returnObject;

});