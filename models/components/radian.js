moduleLoader.imports("radian", [], function () {

	var returnObj = {};

	returnObj.degToRad = function (deg) {
		return deg * (Math.PI/180);
	};
	
	returnObj.radToDeg = function (rad) {
		return rad * (180/Math.PI);
	}
	
	return returnObj;

});