moduleLoader.imports("grid", ['canvas', 'events', 'ec'], function (canvas, events, ec) {

/*======================================================================================
-------------------------------------------DATA----------------------------------------
======================================================================================*/
	var grid = ec();
	
	var ctx = canvas.getContext();

	grid.tileMap = [];
	
	grid.tile = {
		width:32,
		height:32
	};
	
	grid.width = 2500;
	grid.height = 2500;
	
	grid.scroll = {
		x:0,
		y:0
	};
/*======================================================================================
----------------------------------------EVENT HANDLING----------------------------------
======================================================================================*/
	var handleKeyDown = function (event) {

			/***********************************************************
				Because we do not want users to scroll off the map, we ask 
				if there is still room to scroll by the length of a tile.
			***********************************************************/

			if (event['UP']) {
				grid.scroll.y -= ((grid.scroll.y - grid.tile.height) >= 0) ? grid.tile.height : 0;
			}

			if (event['DOWN']) {

				grid.scroll.y += grid.tile.height;

			}

			if (event['LEFT']) {
				grid.scroll.x -= ((grid.scroll.x - grid.tile.width) >= 0) ? grid.tile.width : 0;
			}

			if (event['RIGHT']) {
				grid.scroll.x += grid.tile.width;
			}
	
	};

	var handleClick = function (event) {
		
		var row = Math.floor((event.coordinates.x + grid.scroll.x) / grid.tile.width);
  	var column = Math.floor((event.coordinates.y + grid.scroll.y) / grid.tile.height);

  	if (row < 0 || column < 0) { 
  		return;
  	}

		if (grid.tileMap[row] === null) {
			grid.tileMap[row] = [];
		}

		grid.tileMap[row][column] = 1;

	};

	var handleMouseMove = function (event) {

		var dx = event.origin.x - event.coordinates.x,
				dy = event.origin.y - event.coordinates.y;

	  grid.scroll.x += dx;
	  grid.scroll.y += dy;

	};

	var handleMouseWheel = function (event) {

		if (event.wheelDelta > 0) {
			
			grid.tile.width += 1;
			grid.tile.height += 1;
		
		} else {
		
			grid.tile.width -= 1;
			grid.tile.height -= 1;
		
		}

	};

	var handleInput = function (eventList) {
		
		for (event in eventList) {
		
			if (!eventList.hasOwnProperty(event) || !handle[event]) continue;
			
			handle[event](eventList[event]);
		
		}
	
	};

	events.on('inputs', handleInput);

	var handle = [];

	handle['click']      = handleClick;
	handle['keydown']    = handleKeyDown;
	handle['mousemove']  = handleMouseMove;
	handle['mousewheel'] = handleMouseWheel;
/*======================================================================================
-------------------------------------RENDER----------------------------------------------
======================================================================================*/
	var render = function () {

		var c = canvas();

		ctx.fillStyle = '#ffffff';
		ctx.fillRect(0, 0, c.width, c.height);
		ctx.fillStyle = '#000000';

		var startRow = Math.floor(grid.scroll.x / grid.tile.width);
		var startCol = Math.floor(grid.scroll.y / grid.tile.height);

		var rowCount = startRow + Math.floor(c.width / grid.tile.width) + 1;
		var colCount = startCol + Math.floor(c.height / grid.tile.height) + 1;

		rowCount = (((startRow + rowCount) * grid.tile.width)  > grid.width)  ? (grid.width / grid.tile.width)   : rowCount;
		colCount = (((startCol + colCount) * grid.tile.height) > grid.height) ? (grid.height / grid.tile.height) : colCount;

		for (var row = startRow; row < rowCount; row += 1) {

			for (var col = startCol; col < colCount; col += 1) {

				var tilePositionX = grid.tile.width * row;
				var tilePositionY = grid.tile.height * col;

				tilePositionX -= grid.scroll.x;
				tilePositionY -= grid.scroll.y;

				if (grid.tileMap[row] && grid.tileMap[row][col]) {

					ctx.fillStyle = '#cc0000';
					ctx.fillRect(tilePositionX, tilePositionY, grid.tile.width, grid.tile.height);
					ctx.fillStyle = '#000000';

				} else {

					if ((row % 2) === 0 && (col % 2) === 0) {
						ctx.strokeRect(tilePositionX, tilePositionY, grid.tile.width, grid.tile.height);
					} else {
						ctx.fillRect(tilePositionX, tilePositionY, grid.tile.width, grid.tile.height);
					}

				}

			}

		}

	};

	grid.render = render;
/*======================================================================================
--------------------------------------INITIALIZATION-----------------------------------
======================================================================================*/
	var initializeGrid = function () {

		for (var i = 0; i < grid.width; i += 1) {
			
			grid.tileMap[i] = [];
			
			for (var j = 0; j < grid.height; j += 1) {

				if ((i % 2) === 0 && (j % 2) === 0) {
					grid.tileMap[i][j] = null;	
				} else {
					grid.tileMap[i][j] = 1;
				}

			}
		}

	};

	
	initializeGrid();

	return grid;

});