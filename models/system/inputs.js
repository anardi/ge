moduleLoader.imports('inputs', ['events', 'canvas'], function (events, canvas) {

/***************************************************************************************

				This module registers every input event.  When input events are fired,
				they are normalized and put into a list which represents the current 
				state of inputs.  This list is then dispatched during each game loop.	

***************************************************************************************/

/*======================================================================================
--------------------------------------------DATA----------------------------------------
======================================================================================*/
	var list = [],
			returnObject,
			origin = {
				x:undefined,
				y:undefined
			};

	var inputs = {
		UP:38,
		DOWN:40,
		LEFT:37,
		RIGHT:39
	};
/*======================================================================================
----------------------------------------EVENT CALLBACKS----------------------------------
======================================================================================*/
	var handleKeyDown = function (e) {
		
		list['keydown'] = [];

		switch(e.keyCode) {
		
			case inputs.UP:
				list['keydown']['UP'] = true;
				break
		
			case inputs.DOWN:
				list['keydown']['DOWN'] = true;
				break;
		
			case inputs.LEFT:
				list['keydown']['LEFT'] = true;
				break;
		
			case inputs.RIGHT:
				list['keydown']['RIGHT'] = true;
				break;
		
			default:
				break;
		}
	};

	var handleMouseClick = function (e) {
		
		if (this === canvas()) {
		
			e.coordinates = {
				x : Math.floor(event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - canvas().offsetLeft),
				y : Math.floor(event.clientY + document.body.scrollTop + document.documentElement.scrollTop - canvas().offsetTop)
			}
		
		}

		list['click'] = e;
		
	};

	var handleMouseUp = function (e) {

		if (this === canvas()) {
		
			e.coordinates = {
				x : Math.floor(event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - canvas().offsetLeft),
				y : Math.floor(event.clientY + document.body.scrollTop + document.documentElement.scrollTop - canvas().offsetTop)
			}

			events.off.call(canvas(), 'mousemove', handleMouseMove);

		}
		
		list['mouseup'] = e;
	
	};

	var handleMouseDown = function (e) {

		if (this === canvas()) {

			e.coordinates = {
				x : Math.floor(event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - canvas().offsetLeft),
				y : Math.floor(event.clientY + document.body.scrollTop + document.documentElement.scrollTop - canvas().offsetTop)
			}

			origin.x = e.coordinates.x;
			origin.y = e.coordinates.y;

			events.on.call(canvas(), 'mousemove', handleMouseMove);

		}

		list['mousedown'] = e;

	};

	var handleMouseMove = function (e) {

		if (this === canvas()) {

			e.coordinates = {
				x : Math.floor(event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - canvas().offsetLeft),
				y : Math.floor(event.clientY + document.body.scrollTop + document.documentElement.scrollTop - canvas().offsetTop)
			}

			e.origin = {
				x:origin.x,
				y:origin.y
			}

			origin.x = e.coordinates.x;
			origin.y = e.coordinates.y;

		}

		list['mousemove'] = e;

	};

	var handleScroll = function (e) {
		
		if (e.preventDefault) {
	
			e.preventDefault();
	
		}
		
		e.returnValue = false;
		
		list['mousewheel'] = e;

	};
/*======================================================================================
-------------------------------------------DISPATCH-------------------------------------
======================================================================================*/
	var dispatch = function () {
  
	  if (Object.keys(list).length) {

		  events.fire('inputs', list);

			}

		list = [];
	
	};

	returnObject = function () { return dispatch() };
/*======================================================================================
----------------------------------------EVENT REGISTRATION------------------------------
======================================================================================*/
	events.on('keydown', handleKeyDown);
	events.on.call(canvas(), 'click', handleMouseClick);
	events.on.call(canvas(), 'mouseup', handleMouseUp);
	events.on.call(canvas(), 'mousedown', handleMouseDown);
	events.on.call(window, 'mousewheel', handleScroll);

	returnObject.list   = list;
	
	return returnObject;

});