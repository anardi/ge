moduleLoader.imports('clock', ['update','render', 'inputs', 'events'], function (update, render, inputs, events) {
   
   var returnObject = {},
  
  getNow = Date.now,
  now = 0, last = 0,
  delta = 0,
  dtBuffer = 0,
  
  interval = undefined,
  SIM_RES = 10;

  var loop = function () {
  
  now = getNow();
  delta = (now - last);
  dtBuffer += delta;

  inputs();

  while (dtBuffer >= SIM_RES) {
    update(SIM_RES);
    dtBuffer -= SIM_RES;
  }
  
  render();
  
  };

  var start = function () {
  
  last = getNow();
  interval = setInterval(loop,1);
  
  };

  var stop = function () {
  
  clearInterval(interval);
  
  };

  returnObject.start = start;
  returnObject.stop = stop;

  return returnObject;

});
